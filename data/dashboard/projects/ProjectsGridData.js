import { v4 as uuid } from 'uuid';

// Function to fetch data from the specified URL
const fetchData = async () => {
	try {
	  const response = await fetch('https://dev.landperfect.ai/api/embeddedprojects?format=json', {
		cache: 'no-store', // Set cache control to 'no-store'
		timeout: 5000
	  });
	  if (!response.ok) {
		throw new Error('Failed to fetch data');
	  }
	  return await response.json();
	} catch (error) {
	  console.error('Error fetching data:', error);
	  return [];
	}
  };

  
// Asynchronous function to get data and format it
const ProjectsGridData = async () => {
const apiData = await fetchData();
return apiData.map((item) => ({
	id: uuid(),
	title: item.name,
	category: item.product_name,
	progress: '0', // You may need to adjust this based on the available data from the API
	status: item.enabled ? 'In Progress' : 'Cancel',
	duedate: item.created_at, // You may need to format this based on your requirements
	budget: 0, // You may need to adjust this based on the available data from the API
	icon: 'layout', // You may need to adjust this based on the available data from the API
	team: [], // You may need to adjust this based on the available data from the API
}));
};

export default ProjectsGridData;
