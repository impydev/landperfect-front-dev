import { v4 as uuid } from 'uuid';

// Function to fetch data from the specified URL
const fetchData = async () => {
  try {
    const response = await fetch('https://dev.landperfect.ai/api/embeddedprojects?format=json', {
      cache: 'no-store', // Set cache control to 'no-store'
	  timeout: 5000
    });
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    return await response.json();
  } catch (error) {
    console.error('Error fetching data:', error);
    return [];
  }
};

// Asynchronous function to get data and format it
const ProjectsListData = async () => {
  const apiData = await fetchData();
  return apiData.map((item) => ({
	id: item.id,
	name: item.name,
	created_at: item.created_at,
	description: item.description,
	product_name: item.product_name,
	product_description: item.product_description,
	user_ideas: item.user_ideas,
	traffic_description: item.traffic_description,
	enabled: item.enabled ? 'Enabled' : 'Disabled',
	project_api_id: item.project_api_id,

	progress: '60',
	icon: 'layout'
  }));
};

export default ProjectsListData;
